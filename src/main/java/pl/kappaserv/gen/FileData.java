package pl.kappaserv.gen;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import java.time.ZonedDateTime;

@NoArgsConstructor
@AllArgsConstructor
@Getter
public class FileData {
    private String fileName;
    private Long size;
    private ZonedDateTime creationDate;
}
