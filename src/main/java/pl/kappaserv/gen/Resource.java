package pl.kappaserv.gen;

public interface Resource {
    String fileName = "files.csv";

    void saveOne(FileData fileData, String path);
}