package pl.kappaserv.gen;

import org.springframework.stereotype.Repository;
import java.io.*;

@Repository
public class RecourceImpl implements Resource{

    @Override
    public void saveOne(FileData fileData, String path) {
        String fileUrl = path + Resource.fileName;
        System.out.println(path + "   "  + Resource.fileName);

        try {
            PrintWriter pw = new PrintWriter(new FileOutputStream(new File(fileUrl), true));
            pw.write(fileData.getFileName() + "," + fileData.getSize() + "," + fileData.getCreationDate() + "\n");
            pw.close();

        } catch (FileNotFoundException e) {
            System.out.println("Nie znaleziono pliku " + Resource.fileName);
        } catch (Exception e){
            System.out.println(e.getMessage());
        }
    }
}
