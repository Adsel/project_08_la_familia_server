package pl.kappaserv.gen;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import pl.kappaserv.database.*;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;

@Component
public class DocumentComponentImpl implements DocumentComponent {
    private static final Logger LOGGER = LoggerFactory.getLogger(DocumentComponentImpl.class);

    @Override
    public void createDocument(Receipt receiptDataDto, String fileDestination) {
        try {
            Document document = new Document();
            PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(fileDestination));
            document.open();

            Paragraph p1 = new Paragraph(
                     "------------------------------------" + "\n" +
                            "           Paragon Fiskalny         " + "\n" +
                            "------------------------------------" + "\n"
            );
            p1.setAlignment(Element.ALIGN_CENTER);
            document.add(p1);

            ArrayList<CompletedPizza> pizzas = receiptDataDto.getPizzas();
            if(pizzas.size() > 0) {
                PdfPTable table = newTable();
                Paragraph p2 = new Paragraph(
                         "------------------------------------" + "\n" +
                                "               Pizze:               " + "\n" +
                                "------------------------------------" + "\n"
                );
                p2.setAlignment(Element.ALIGN_CENTER);
                document.add(p2);

                for(int i = 0; i < pizzas.size(); i++){
                    CompletedPizza p = pizzas.get(i);
                    table.addCell(createCell(p.getCode(), 1, Element.ALIGN_LEFT));
                    table.addCell(createCell(p.getName(), 1, Element.ALIGN_LEFT));
                    table.addCell(createCell(String.valueOf(p.getPriceD() / 100.0), 1, Element.ALIGN_LEFT));
                }
                document.add(table);
            }

            ArrayList<Drink> drinks = receiptDataDto.getDrinks();
            if(drinks.size() > 0) {
                PdfPTable table = newTable();
                Paragraph p3 = new Paragraph(
                        "------------------------------------" + "\n" +
                                "             Napoje:              " + "\n" +
                                "------------------------------------" + "\n"
                );
                p3.setAlignment(Element.ALIGN_CENTER);
                document.add(p3);

                for(int i = 0; i < drinks.size(); i++){
                    Drink d = drinks.get(i);
                    table.addCell(createCell(d.getCode(), 1, Element.ALIGN_LEFT));
                    table.addCell(createCell(d.getName(), 1, Element.ALIGN_LEFT));
                    table.addCell(createCell(String.valueOf(d.getPriceD() / 100.0), 1, Element.ALIGN_LEFT));
                }
                document.add(table);
            }

            ArrayList<CompletedSet> sets = receiptDataDto.getSets();
            if(sets.size() > 0) {
                PdfPTable table = newTable();
                Paragraph p4 = new Paragraph(
                        "------------------------------------" + "\n" +
                                "             ZESTAWY:             " + "\n" +
                                "------------------------------------" + "\n"
                );
                p4.setAlignment(Element.ALIGN_CENTER);
                document.add(p4);
                for(int i = 0; i < sets.size(); i++){
                    CompletedSet s = sets.get(i);
                    table.addCell(createCell(s.getCode(), 1, Element.ALIGN_LEFT));
                    table.addCell(createCell(s.getName(), 1, Element.ALIGN_LEFT));
                    table.addCell(createCell(String.valueOf(s.getPriceD() / 100.0), 1, Element.ALIGN_LEFT));
                }
                document.add(table);
            }

            document.add(new Paragraph("------------------------------------"));
            document.add(new Paragraph("             Do zapłaty:            "));
            document.add(new Paragraph("------------------------------------"));
            document.add(new Paragraph(receiptDataDto.getPrice().toString()));

            if(receiptDataDto.getPrice() > 100.0){
                document.add(new Paragraph(DataBase.promotions.get(2).getName()));
                double tempP = Math.round(receiptDataDto.getPriceI() * 0.8) / 100.0;
                document.add(new Paragraph(String.valueOf(tempP)));
            }

            int counterOfWholePizzas = 0;
            for(CompletedPizza cp: receiptDataDto.getPizzas()){
                if(cp.getWhole()) {
                    counterOfWholePizzas += 2;
                }
                else {
                    counterOfWholePizzas += 1;
                }
                if(counterOfWholePizzas >= 4){
                    document.add(new Paragraph(""));
                    document.add(new Paragraph("------------------------------------"));
                    document.add(new Paragraph("              Promocje:             "));
                    document.add(new Paragraph("------------------------------------"));

                    document.add(new Paragraph(DataBase.promotions.get(1).getName()));
                    document.add(new Paragraph(DataBase.promotions.get(1).getDescription()));
                    Drink extraDrink = DataBase.drinks.get(0);
                    document.add(new Paragraph(extraDrink.getName() + " " + extraDrink.getVolume()));
                    break;
                }
            }

            setBackgroundAsGradient(document, writer);
            document.close();
        } catch (DocumentException | FileNotFoundException e) {
            LOGGER.error("i can't create document or file not exists");
        }
    }

    private PdfPCell createCell(String content, float borderWidth, int alignment) {
        final String FONT = "static/arial.ttf";
        Font font = FontFactory.getFont(FONT, BaseFont.IDENTITY_H, true);
        PdfPCell cell = new PdfPCell(new Phrase(content, font));
        cell.setBorderWidth(borderWidth);
        cell.setHorizontalAlignment(alignment);
        cell.setPaddingTop(3);
        cell.setPaddingBottom(6);
        cell.setPaddingLeft(3);
        cell.setPaddingRight(3);

        return cell;
    }

    private void setBackgroundAsGradient(Document document, PdfWriter writer) {
        Rectangle pageSize = document.getPageSize();
        PdfShading axial = PdfShading.simpleAxial(writer,
                pageSize.getLeft(pageSize.getWidth() / 10), pageSize.getBottom(),
                pageSize.getRight(pageSize.getWidth() / 10), pageSize.getBottom(),
                new BaseColor(250, 193, 255),
                new BaseColor(0, 153, 255), true, true);
        PdfContentByte canvas = writer.getDirectContentUnder();
        canvas.paintShading(axial);
    }

    private PdfPTable newTable() throws DocumentException {
        PdfPTable table = new PdfPTable(3);
        table.setWidths(new int[]{1, 2, 1});
        table.addCell(createCell("KOD", 2, Element.ALIGN_LEFT));
        table.addCell(createCell("Nazwa", 2, Element.ALIGN_LEFT));
        table.addCell(createCell("Cena", 2, Element.ALIGN_LEFT));

        return table;
    }
}
