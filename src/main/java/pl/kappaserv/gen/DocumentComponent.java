package pl.kappaserv.gen;

import pl.kappaserv.database.Receipt;

public interface DocumentComponent {
    void createDocument(Receipt ReceiptDataDto, String fileDestination);
}
