package pl.kappaserv.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.kappaserv.database.*;
import pl.kappaserv.gen.FileData;
import pl.kappaserv.service.BaseService;
import pl.kappaserv.service.FileService;
import pl.kappaserv.service.impl.BaseServiceImpl;
import java.util.List;

@RestController
@RequestMapping("/api")
public class PizzaApiController {
    private static final Logger LOGGER = LoggerFactory.getLogger(PizzaApiController.class);

    @Autowired
    private BaseService base = new BaseServiceImpl();

    @Autowired
    private FileService fileService;

    @CrossOrigin
    @GetMapping(value = "/server-test")
    public ResponseEntity<String> serverTest() {
        LOGGER.info("--- sprawdzono działanie serwera");

        return new ResponseEntity<>("połączenie z serwerem", HttpStatus.OK);
    }

    @CrossOrigin
    @GetMapping(value = "/products/get-pizzas")
    public ResponseEntity<List<Pizza>> getPizzas() {
        List<Pizza> list = base.getPizzas();
        LOGGER.info("--- wyslano pizze");

        return new ResponseEntity<>(list, HttpStatus.OK);
    }

    @CrossOrigin
    @GetMapping(value = "/products/get-pizza-{id}")
    public ResponseEntity<Pizza> getPizza(@PathVariable("id") Integer id) {
        Pizza pizza = base.getPizza(id);

        if(pizza == null) {
            LOGGER.info("--- nie znaleziono pizzy o ID = " + id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        else{
            LOGGER.info("--- wyslano pizze o ID = " + id);
            return new ResponseEntity<>(pizza, HttpStatus.OK);
        }
    }

    @CrossOrigin
    @GetMapping(value = "/products/get-drinks")
    public ResponseEntity<List<Drink>> getDrinks() {
        List<Drink> list = base.getDrinks();
        LOGGER.info("--- wyslano napoje");

        return new ResponseEntity<>(list, HttpStatus.OK);
    }

    @CrossOrigin
    @GetMapping(value = "/products/get-drink-{id}")
    public ResponseEntity<Drink> getDrink(@PathVariable("id") Integer id) {
        Drink drink = base.getDrink(id);

        if(drink == null) {
            LOGGER.info("--- nie znaleziono napoju o ID = " + id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        else{
            LOGGER.info("--- wyslano napoj o ID = " + id);
            return new ResponseEntity<>(drink, HttpStatus.OK);
        }
    }

    @CrossOrigin
    @GetMapping(value = "/products/get-promotions")
    public ResponseEntity<List<Promotion>> getPromotions() {
        List<Promotion> list = base.getPromotions();
        LOGGER.info("--- wyslano promocje");

        return new ResponseEntity<>(list, HttpStatus.OK);
    }

    @CrossOrigin
    @GetMapping(value = "/products/get-additions")
    public ResponseEntity<List<Addition>> getAdditions(){
        List<Addition> additions = base.getAdditions();
        LOGGER.info("--- wyslano dodatki");

        return new ResponseEntity<>(additions, HttpStatus.OK);
    }

    @CrossOrigin
    @GetMapping(value = "/products/get-addition-{id}")
    public ResponseEntity<Addition> getAddition(@PathVariable("id") Integer id) {
        Addition addition = base.getAddition(id);

        if(addition == null) {
            LOGGER.info("--- nie znaleziono dodatku o ID = " + id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        else{
            LOGGER.info("--- wyslano dodatek o ID = " + id);
            return new ResponseEntity<>(addition, HttpStatus.OK);
        }
    }

    @CrossOrigin
    @GetMapping(value = "/products/get-sauces")
    public ResponseEntity<List<Sauce>> getSauces(){
        List<Sauce> sauces = base.getSauces();
        LOGGER.info("--- wyslano dodatki");

        return new ResponseEntity<>(sauces, HttpStatus.OK);
    }

    @CrossOrigin
    @GetMapping(value = "/products/get-sauce-{id}")
    public ResponseEntity<Sauce> getSauce(@PathVariable("id") Integer id) {
        Sauce sauce  = base.getSauce(id);

        if(sauce == null) {
            LOGGER.info("--- nie znaleziono sosu o ID = " + id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        else{
            LOGGER.info("--- wyslano sos o ID = " + id);
            return new ResponseEntity<>(sauce, HttpStatus.OK);
        }
    }

    @CrossOrigin
    @GetMapping(value = "/products/get-sets")
    public ResponseEntity<List<Set>> getSets(){
        List<Set> sets = base.getSets();
        LOGGER.info("--- wyslano zestawy");

        return new ResponseEntity<>(sets,HttpStatus.OK);
    }

    @CrossOrigin
    @GetMapping(value = "/products/get-set-{id}")
    public ResponseEntity<Set> getSet(@PathVariable("id") Integer id) {
        Set set = base.getSet(id);

        if(set == null) {
            LOGGER.info("--- nie znaleziono zestawu o ID = " + id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        else{
            LOGGER.info("--- wyslano zestaw o ID = " + id);
            return new ResponseEntity<>(set, HttpStatus.OK);
        }
    }

    @CrossOrigin
    @PostMapping(value = "/files/create-file")
    public ResponseEntity<?> createFile(@RequestBody Receipt receipt) {
        FileData fileData = fileService.createFile(receipt, fileService.PATH);
        LOGGER.info("--- create pdf file for user: " + receipt.getPrice() + " at name: " + fileData.getFileName());

        return fileData != null ?
                new ResponseEntity<>(fileData, HttpStatus.CREATED) :
                new ResponseEntity<>("Nie udało się utworzyć pliku", HttpStatus.NO_CONTENT);
    }
}
