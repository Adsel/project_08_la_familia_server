package pl.kappaserv.service;

import pl.kappaserv.database.*;
import java.util.ArrayList;

public interface BaseService {
    ArrayList<Pizza> getPizzas();
    Pizza getPizza(Integer id);
    ArrayList<Drink> getDrinks();
    Drink getDrink(Integer id);
    ArrayList<Addition> getAdditions();
    Addition getAddition(Integer id);
    ArrayList<Sauce> getSauces();
    Sauce getSauce(Integer id);
    ArrayList<Promotion> getPromotions();
    ArrayList<Set> getSets();
    Set getSet(Integer id);
}
