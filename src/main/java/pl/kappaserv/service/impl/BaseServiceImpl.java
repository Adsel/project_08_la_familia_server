package pl.kappaserv.service.impl;

import org.springframework.stereotype.Service;
import pl.kappaserv.database.*;
import pl.kappaserv.service.BaseService;
import java.util.ArrayList;

@Service
public class BaseServiceImpl implements BaseService {
    private ArrayList<Pizza> pizzas;
    private ArrayList<Drink> drinks;
    private ArrayList<Sauce> sauces;
    private ArrayList<Addition> additions;
    private ArrayList<Promotion> promotions;
    private ArrayList<Set> sets;

    public BaseServiceImpl(){
        pizzas = DataBase.pizzas;
        drinks = DataBase.drinks;
        sauces = DataBase.sauces;
        additions = DataBase.additions;
        promotions = DataBase.promotions;
        sets = DataBase.sets;
    }

    @Override
    public ArrayList<Pizza> getPizzas() { return pizzas; }

    @Override
    public Pizza getPizza(Integer id){
        id -= 1;
        if(id < 0 || id >= this.pizzas.size()){
            return null;
        }
        else {
            return this.pizzas.get(id);
        }
    }

    @Override
    public ArrayList<Drink> getDrinks() { return drinks; }

    @Override
    public Drink getDrink(Integer id){
        id -= 1;
        if(id < 0 || id >= this.drinks.size()){
            return null;
        }
        else {
            return this.drinks.get(id);
        }
    }

    @Override
    public ArrayList<Sauce> getSauces() { return sauces; }

    @Override
    public Sauce getSauce(Integer id){
        id -= 1;
        if(id < 0 || id >= this.sets.size()){
            return null;
        }
        else {
            return this.sauces.get(id);
        }
    }

    @Override
    public ArrayList<Addition> getAdditions() { return additions; }

    @Override
    public Addition getAddition(Integer id){
        id -= 1;
        if(id < 0 || id >= this.sets.size()){
            return null;
        }
        else {
            return this.additions.get(id);
        }
    }

    @Override
    public ArrayList<Promotion> getPromotions() { return promotions; }

    @Override
    public ArrayList<Set> getSets(){ return sets; }

    @Override
    public Set getSet(Integer id){
        id -= 1;
        if(id < 0 || id >= this.sets.size()){
            return null;
        }
        else {
            return this.sets.get(id);
        }
    }
}