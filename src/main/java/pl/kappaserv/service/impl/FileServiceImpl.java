package pl.kappaserv.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.kappaserv.database.Receipt;
import pl.kappaserv.gen.DocumentComponent;
import pl.kappaserv.gen.FileData;
import pl.kappaserv.gen.Resource;
import pl.kappaserv.service.FileService;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;
import java.time.ZonedDateTime;

@Service
public class FileServiceImpl implements FileService {
    private static final Logger LOGGER = LoggerFactory.getLogger(FileServiceImpl.class);

    @Autowired
    private Resource resource;

    @Autowired
    private DocumentComponent documentComponent;

    @Override
    public FileData createFile(Receipt receipt, String path) {
        String fileName = "paragon" + "ID" + ".pdf";
        String fileDestination = path + fileName;

        try {
            Files.createDirectories(Paths.get(path));
            documentComponent.createDocument(receipt, fileDestination);
            FileData fileData = new FileData(fileName, getFileSize(fileDestination), ZonedDateTime.now());
            resource.saveOne(fileData, path);

            return fileData;
        } catch (IOException e) {
            LOGGER.info("Nie można zapisać danych!");
        }

        return null;
    }

    private Long getFileSize(String fileDestination) throws IOException {
        Path filePath = Paths.get(fileDestination);
        BasicFileAttributes fileAttributes = Files.readAttributes(filePath, BasicFileAttributes.class);

        return fileAttributes.size();
    }
}
