package pl.kappaserv.service;

import pl.kappaserv.database.Receipt;
import pl.kappaserv.gen.FileData;

public interface FileService {
    String PATH = "D:\\pdf\\";

    FileData createFile(Receipt receipt, String path);
}
