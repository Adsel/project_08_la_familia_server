package pl.kappaserv.database;

import lombok.Getter;
import org.springframework.stereotype.Component;
import java.util.ArrayList;

@Getter
@Component
public class Receipt {
    private Integer price;
    private ArrayList<Drink> drinks;
    private ArrayList<CompletedSet> sets;
    private ArrayList<CompletedPizza> pizzas;

    public Receipt(ArrayList<Drink> drinks, ArrayList<CompletedSet> sets, ArrayList<CompletedPizza> pizzas){
        this.drinks = drinks;
        this.sets = sets;
        this.pizzas = pizzas;
        setPrice();
    }

    public Receipt(){
        price = 0;
    }

    private void setPrice(){
        Integer p = 0;
        for(CompletedPizza cp: pizzas){
            System.out.println(cp.getPrice());
            p += cp.getPrice();
        }

        for(Drink cd: drinks){
            p += cd.getPrice();
        }

        for(CompletedSet cs: sets){
            p += cs.getPrice();
        }

        this.price = p;
    }

    public Double getPrice(){
        return (this.price / 100.0);
    }

    public Integer getPriceI(){
        return  this.price;
    }
}
