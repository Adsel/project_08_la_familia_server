package pl.kappaserv.database;

import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;

@Getter
@Setter
@Component
public class Pizza extends Product {
    private String size;
    private String composition;

    public Pizza(){
        super();
    }

    public Pizza(Integer id, Integer price, String name, String desc, String code, String size, String composition){
        super(id, price, name, desc, code);
        this.size = size;
        this.composition = composition;
    }
}