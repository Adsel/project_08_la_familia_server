package pl.kappaserv.database;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Component
public abstract class Product {
    private Integer id;
    private Integer price;
    private String name;
    private String desc;
    private String code;

    public double getPriceD(){
        Double d = new Double(this.getPrice());
        d = (Math.round(d * 100.0) / 100.0);

        return d;
    }

    public Integer getPrice(){
        return this.price;
    }
}
