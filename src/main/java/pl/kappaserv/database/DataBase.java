package pl.kappaserv.database;

import java.util.ArrayList;
import java.util.Arrays;

public class DataBase {
    public static ArrayList<Pizza> pizzas = new ArrayList<>(Arrays.asList(
            new Pizza(1,2450, "Margherita", "Klasyczna pizza pachnaca swiezymi pomidorami", "K101", "L", "pomidory, mozarella, bazylia"),
            new Pizza(2, 2600, "Spinachi", "Delikata i aromatyczna", "K102", "XL", "szpinak, suszone pomidory, czosnek, gorgonzola"),
            new Pizza(3,2700, "Sezonowa: Smak jesieni", "Pikanktna, z kawalkami dyni, i marchwi", "K103", "XL", "dynia, marchew, suszone pomidory, chili, oregano"),
            new Pizza(4,3050, "Sezonowa: Caponata sycylijska", "Wyrafinowana kompozycja smakow kuchni sycylijskiej", "K104", "L", "pomidory, caponata (oliwki, baklazan, kapary, anchois"),
            new Pizza(5,3150, "Chorizo", "Pikantna z nuta dymu", "K105", "XL", "sos pomidorowy, wedzony ser scamorza, mozarella"),
            new Pizza(6,3200, "Ser kozi", "Delikatne polaczenie koziego sera i gruszki", "K106", "XXL", "sos serowy, ser kozi, gruszka, rukola, figa"),
            new Pizza(7,2850, "Diavola", "Klasyczna, pikantna pizza z saliami i jalapenio", "K107", "L", "sos pomidorowy, salami, pieczarki, jalapenio, mozarella, papryka"),
            new Pizza(8,3200, "Prosciutto di Parma", "Klasyczna wloska pizza z szynka parmenska", "K108", "XL", "sos pomidorowy, mozarella, rukola, prosciutto, parmezan" ),
            new Pizza(9,3400, "La familia", "Dawna receptura rodzinna", "K109", "L", "pomidory, mozarella, bazylia, salami, papryka, jalapenio, anchois"),
            new Pizza(10,3100, "Wegetariana", "aromatyczna, z bukietem warzyw ", "K110", "XL", "pomidory, mozarella, brokuly, pieczrki, cebula, bazylia")
    ));

    public static ArrayList<Drink> drinks = new ArrayList<>(Arrays.asList(
            new Drink(1,600, "Cola", "Coca-cola", "K201", "250ml"),
            new Drink(2,800, "Sok pomaranczowy", " Swiezo wyciskany sok pomaranczowy", "K202", "330ml"),
            new Drink(3,800, "Sok jablkowy", "Swiezo wyciskany sok jablkowy", "K203", "200ml"),
            new Drink(4,1000, "Piwo orkiszowe", "Piwo orkiszowe", "K204", "150ml"),
            new Drink(5,900, "Piwo Zawiec", "Piwo Zawiec", "K205", "250ml"),
            new Drink(6,1200, "Wino czerwone", "Tempranillo, wytrawne", "K206", "100ml"),
            new Drink(7,1200, "Wino biale", "Riesling, wytrawne", "K206", "150ml")

    ));

    public static ArrayList<Set> sets = new ArrayList<>(Arrays.asList(
            new Set(1, 2200,"Kola i Margarita", "Dobre dla tych co im filet ucieka", "K301",
                    new ArrayList<Pizza>(
                            Arrays.asList(
                                pizzas.get(0)
                            )
                    ),
                    new ArrayList<Drink>(
                        Arrays.asList(
                        drinks.get(0)
                        )
                    )
            ),
            new Set(2, 2200,"Marcinowsky", "Dla Marcinów którzy lubią dobrze zjeść", "K302",
                    new ArrayList<Pizza>(
                            Arrays.asList(
                                    pizzas.get(8),
                                    pizzas.get(5),
                                    pizzas.get(2)
                            )
                    ),
                    new ArrayList<Drink>(
                            Arrays.asList(
                                    drinks.get(0),
                                    drinks.get(0),
                                    drinks.get(5)
                            )
                    )
            ),
            new Set(2, 2200,"Party-pack", "W sam raz na imprezę wśród znajomych!", "K303",
                    new ArrayList<Pizza>(
                            Arrays.asList(
                                    pizzas.get(9),
                                    pizzas.get(3),
                                    pizzas.get(2),
                                    pizzas.get(7)
                            )
                    ),
                    new ArrayList<Drink>(
                            Arrays.asList(
                                    drinks.get(0),
                                    drinks.get(1),
                                    drinks.get(5),
                                    drinks.get(2),
                                    drinks.get(3)
                            )
                    )
            )
    ));

    public static ArrayList<Addition> additions = new ArrayList<>(Arrays.asList(
            new Addition(1,900, "Salatka avokado", "Salatka z kurczakiem i avokado", "K901"),
            new Addition(2,1050, "Beef salad", "Salatka z wolowina, rukola i pomarancza", "K902"),
            new Addition(3,750, "Salatka wiosenna", "Salatka z pomidorem, ogrkiem i oliwkami", "K903"),
            new Addition(4,600, "Pieczone ziemniaki", "Pieczone ziemniaki w mundurkach z sosem tymiankowym", "K904"),
            new Addition(5,700, "Warzywa na parze", "Brokuly, marchew, por, seler gotowanye na parze", "K905"),
            new Addition(6,800, "Chrupiace warzywa", "Pieczone buraki, marchew, seler i piertuszka", "K906")
    ));

    public static ArrayList<Sauce> sauces = new ArrayList<>(Arrays.asList(
            new Sauce(1, 400, "Pomidorowy", "swieze pomidory z bazylia", "K801"),
            new Sauce(2,400, "Czosnkowy", "jogurt, czosnek, oregano", "K802"),
            new Sauce(3, 400, "Pikantny", "pomidory, pieprz koorowy, jalapenio, chili", "K803")

    ));

    public static ArrayList<Promotion> promotions = new ArrayList<>(Arrays.asList(
            new Promotion("50/50", "Możliwość zamówienia połówek pizzy", "nieograniczona ilość 0.5 pizz"),
            new Promotion("Drink it!", "Napój gratis przy zakupie dwóch pizz", "min. dwie pizze"),
            new Promotion("-20%", "Rabat 20% przy rachunku powyżej 100zl", "zamowienie powyzej 100zl")
    ));
}