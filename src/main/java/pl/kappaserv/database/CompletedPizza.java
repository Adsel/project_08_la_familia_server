package pl.kappaserv.database;

import java.util.ArrayList;
import lombok.Getter;
import org.springframework.stereotype.Component;

@Getter
@Component
public class CompletedPizza extends Pizza {
    private ArrayList<Addition> additions;
    private ArrayList<Sauce> sauces;
    private Boolean whole;

    public CompletedPizza(){
        super();
    }

    public CompletedPizza(Integer id, Integer price, String name, String desc, String code, String size, String composition, ArrayList<Addition> additions, ArrayList<Sauce> sauces, Boolean whole){
        super(id, price, name, desc, code, size , composition);
        this.additions = additions;
        this.sauces = sauces;
        this.whole = whole;
    }

    @Override
    public Integer getPrice() {
        Integer p = super.getPrice();
        for(Addition a: additions){
            p += a.getPrice();
        }

        for(Sauce s: sauces){
            p += s.getPrice();
        }

        if(this.whole){
            return p;
        }
        else{
            return Math.round(p / 2);
        }
    }
}
