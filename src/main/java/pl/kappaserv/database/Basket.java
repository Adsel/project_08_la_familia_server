package pl.kappaserv.database;

import java.io.Serializable;
import java.util.ArrayList;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

@NoArgsConstructor
@Getter
@Component
public class Basket implements Serializable {
    private ArrayList<CompletedPizza> pizzas = new ArrayList<>();
    private ArrayList<Drink> drinks = new ArrayList<>();
    private ArrayList<CompletedSet> sets = new ArrayList<>();

    public void addCompletedPizza(CompletedPizza cp){
        pizzas.add(cp);
    }

    public void addDrink(Drink cd){
        drinks.add(cd);
    }

    public void addSet(CompletedSet cs){
        sets.add(cs);
    }

    public void removePizza(CompletedPizza cp){
        pizzas.remove(cp);
    }

    public void removeDrink(CompletedDrink cd){
        pizzas.remove(cd);
    }

    public void removeSet(CompletedSet cs){
        pizzas.remove(cs);
    }


    @Override
    public String toString() {
        String result = "Basket{";
        for(CompletedPizza cp: pizzas){
            result += "Pizza{" +
                    "name='" + cp.getName() + '\'' +
                    "desc='" + cp.getDesc() + '\'' +
                    "size='" + cp.getSize() + '\'' +
                    "code='" + cp.getCode() + '\'' +
                    "price='" + cp.getPrice() + '\'' +
                    "composition='" + cp.getComposition() + '\'' +
                    "}";
        }
        result += '}';

        return result;
    }
}