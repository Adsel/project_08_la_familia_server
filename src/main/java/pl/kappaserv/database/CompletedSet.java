package pl.kappaserv.database;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.stereotype.Component;
import java.util.ArrayList;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Component
public class CompletedSet extends Product{
    private ArrayList<CompletedPizza> pizzas;
    private ArrayList<Drink> drinks;

    public Integer getPrice(){
        Integer price = 0;
        for(CompletedPizza cp: pizzas){
            price += cp.getPrice();
        }

        for(Drink cd: drinks) {
            price += cd.getPrice();
        }

        return price;
    }
}