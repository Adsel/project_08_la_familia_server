package pl.kappaserv.database;

import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;

@Getter
@Setter
@Component
public class CompletedDrink extends Product {
    private String volume;

    public CompletedDrink(){
        super();
    }

    public CompletedDrink(Integer id, Integer price, String name, String desc, String code, String volume){
        super(id, price, name, desc, code);
        this.volume = volume;
    }
}