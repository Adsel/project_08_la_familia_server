package pl.kappaserv.database;

import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;

@Getter
@Setter
@Component
public class Sauce extends Product {
    public Sauce(){
        super();
    }

    public Sauce(Integer id, Integer price, String name, String desc, String code){
        super(id, price, name, desc, code);
    }
}